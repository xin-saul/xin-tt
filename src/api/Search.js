import request from '@/utils/request'

// 搜索 - 获取搜索结果
// eslint-disable-next-line
export const searchResultAPI = ({ page = 1, per_page = 10, q }) => request({
  url: '/v1_0/search',
  params: {
    page,
    per_page,
    q
  }
})

// 搜索 - 联想菜单
export const suggestListAPI = ({ keyWord }) => request({
  url: '/v1_0/suggestion',
  params: {
    q: keyWord
  }
})
