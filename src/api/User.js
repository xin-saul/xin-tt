import request from '@/utils/request'

// 用户 - 获取基本信息（我的页面显示数据）
export const getUserInfoAPI = () => request({
  url: '/v1_0/user'
})

// 用户 - 获取用户个人资料
export const userProfileAPI = () => request({
  url: '/v1_0/user/profile'
})

// 用户 - 更新头像
export const updateUserPhotoAPI = (fd) => request({
  url: '/v1_0/user/photo',
  method: 'PATCH',
  data: fd // 一会传进来的new FromData
  // 请求体直接是FormData表单对象，也不需要添加Content-Type
  // Content-Type: application/json; axios携带的，前提：data请求体是对象 -> json字符串 -> 发给后台
  // Content-Type: multipart/from-data; 浏览器携带的，前提：data请求体必须是FormData类型对象
})

// 用户 - 更新基本资料
export const unpdateUserProfileAPI = (dataobj) => {
  // 判断，有值才带参数名给后台，无参数名不携带
  // 写法1：结构赋值，4个判断，往空对象上添加有值的加上去
  // 写法2：外面想传几对key+value，就直接传人交给后台，但是不够语义化
  // 下为写法3
  const obj = {
    name: '',
    gender: 0,
    birthday: '',
    real_name: '',
    intro: ''
  }

  for (const prop in obj) { // 遍历参数对象里每个key
    if (dataobj[prop] === undefined) { // 用key去外面传入的参数对象匹配，如果没有找到（证明外面没传这个参数）
      delete obj[prop] // 从obj身上移除这个参数
    } else {
      obj[prop] = dataobj[prop] // 如果使用了，就从外面对象取出对应key值，保存到obj上
    }
  }

  return request({
    url: '/v1_0/user/profile',
    method: 'PATCH',
    data: obj
  })
}
