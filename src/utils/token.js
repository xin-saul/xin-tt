// 封装token的三个方法->专门用于token
const key = 'xin-tt'

export const setToken = (token) => {
  localStorage.setItem(key, token)
}

export const getToken = () => localStorage.getItem(key)

export const removeToken = () => {
  localStorage.removeItem(key)
}
