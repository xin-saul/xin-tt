import '@/utils/console' // 去掉打印语句
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入plugins插件
import plugins from '@/plugins'
// 代码高亮的样式
import 'highlight.js/styles/default.css'
// 引入flexible.js -> 设置根标签字体大小（移动端适配）
import 'amfe-flexible'
// 引入vant组件注册
import '@/plugins/vantComponent'
Vue.use(plugins)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
