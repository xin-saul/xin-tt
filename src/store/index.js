import Vue from 'vue'
import Vuex from 'vuex'
import { setStorage, getStorage } from '@/utils/storage'

Vue.use(Vuex)

export default new Vuex.Store({
  // 准备actions——用于响应组件中的动作
  actions: {
  },
  // 准备mutations——用于操作数据（state）
  mutations: {
    // 使用传进来的头像地址给聊天界面和我的界面我的头像
    SET_USERPHOTO (state, value) {
      state.userPhoto = value
      setStorage('userPhoto', JSON.stringify(state.userPhoto))
    },
    SET_USERNAME (state, value) {
      state.userName = value
      setStorage('userName', JSON.stringify(state.userName))
    }
  },
  // 准备state——用于存储数据
  state: {
    // 头像图片默认值
    userPhoto: JSON.parse(getStorage('userPhoto')) || 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201810%2F18%2F20181018162634_pqpie.thumb.700_0.jpg&refer=http%3A%2F%2Fb-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1660817293&t=01cae6f033f3bf86c423aa79db1e9d64',
    userName: JSON.parse(getStorage('userName')) || '起个名字吧'
  },
  // 准备getters对象——用于将state中的数据进行加工
  getters: {
  },
  modules: {
  }
})
