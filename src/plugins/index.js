// 对Vue的全局指令进行封装
export default {
  install (Vue) {
    // 全局自定义指令
    Vue.directive('fofo', {
      // 指令所在元素插入页面时,element代表指令所在标签
      inserted (element, binding) {
        focusFn(element)
      },
      // 指令所在的模板被重新解析时
      update (element, binding) {
        setTimeout(() => {
          focusFn(element)
        }, 500)
      }
    })
  }
}

// input 和 textarea 以及vant组件库内部的input自动聚焦
function focusFn (element) {
  // 原生DOM.nodeName 拿到标签名字 （大写的字符串）
  if (element.nodeName === 'TEXTAREA' || element.nodeName === 'INPUT') {
    element.focus()
  } else {
    // element本身不是输入框，尝试往里面获取
    const theInput = element.querySelector('input')
    const theTextarea = element.querySelector('textarea')
    // 不一定能获取得到，有值了再执行focus()
    if (theInput) theInput.focus()
    if (theTextarea) theTextarea.focus()
  }
}
