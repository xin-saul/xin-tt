// 基于vant进行二次封装 / 封装一个.vue文件组件(弹窗)
// 封装通知的"方法"
// import { Notify } from 'vant'
// export default Notify

import { Toast } from 'vant'
// 外面逻辑页面传人的字段，使用自定义函数解构赋值形参中转接收
// 内部如何使用和传值，在这个函数体里决定
export default ({ type, message }) => {
  if (type === 'warning' || type === 'danger') { // 失败的图标类型叫fail才行
    type = 'fail'
  }
  Toast({
    type,
    message
  })
}
