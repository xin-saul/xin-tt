import request from '@/utils/request'

// 新闻 - 点赞
export const likeArticleAPI = ({ artId }) => request({
  url: '/v1_0/article/likings',
  method: 'POST',
  data: {
    target: artId
  }
})

// 新闻 - 取消点赞
export const unLikeArticleAPI = ({ artId }) => request({
  url: `/v1_0/article/likings/${artId}`,
  method: 'DELETE'
})

// 新闻 - 获取文章详情
export const detailAPI = ({ artId }) => request({
  url: `/v1_0/articles/${artId}`
})

// 用户 - 关注用户
export const userFollowedAPI = ({ userId }) => request({
  url: '/v1_0/user/followings',
  method: 'POST',
  data: {
    target: userId
  }
})

// 用户 - 取消关注用户
export const userUnFollowedAPI = ({ userId }) => request({
  url: `/v1_0/user/followings/${userId}`,
  method: 'DELETE'
})

// 评论 - 对文章或者评论进行评论
// eslint-disable-next-line
export const commentSendAPI = ({ id, content, art_id = null }) => {
  // 1.axios中，data请求体传参，如果值为null是不会忽略这个参数的，会把参数名和值携带给后台（只有params遇到null才会忽略）
  // 2.形参art_id是可选参数，如果逻辑页面上对文章评论，则不需要传递art_id
  // 所以这时，内部art_id值为null就证明此次调用，是针对文章评论
  const obj = {
    target: id,
    content
  }
  // eslint-disable-next-line
  if(art_id !== null) {
    // eslint-disable-next-line
    obj.art_id = art_id
  }
  return request({
    url: '/v1_0/comments',
    method: 'POST',
    data: obj
  })
}

// 评论 - 获取评论或评论回复
export const commentsListAPI = ({ artId, offset = null, limit = 10 }) => request({
  url: '/v1_0/comments',
  params: { // axios只针对params参数，如果发现键值对，值为null, 会忽略此参数名和值不携带在url?后面
    type: 'a',
    source: artId,
    offset,
    limit
  }
})

// 评论 - 文章 - 喜欢
export const commentLikingAPI = ({ comId }) => request({
  url: '/v1_0/comment/likings',
  method: 'POST',
  data: {
    target: comId
  }
})

// 评论 - 文章 - 不喜欢
export const commentDisLikingAPI = ({ comId }) => request({
  url: `/v1_0/comment/likings/${comId}`,
  method: 'DELETE'
})
