import Vue from 'vue'
import VueRouter from 'vue-router'
import { getToken } from '@/utils/token'
// 路由懒加载：为了让第一个页面，加载的app.js小一点，打开网页快一点
// 思路：把组件对应js，分成若干个.js，路由切换到哪个页面，才去加载对应的.js文件
// 原因：webpack分析入口时，发现router里上来就import所有页面，所以直接打包进app.js -> 很大
// 解决：当路由路径匹配规则时，才现去import引入对应的组件js文件
Vue.use(VueRouter)

const routes = [
  // 配置根路由并配置重定向
  {
    path: '/',
    redirect: '/layout/home'
  },
  { // 登陆界面路由
    path: '/login',
    component: () => import(/* webpackChunkName: "Login" */ '@/views/Login'),
    // 独享守卫，特定路由切换之后被调用
    beforeEnter: (to, from, next) => {
      if (getToken()?.length > 0) {
        next('/layout/home')
      } else {
        next()
      }
    }
  },
  { // 主页
    path: '/layout',
    component: () => import(/* webpackChunkName: "Layout" */ '@/views/Layout'),
    children: [
      { // 首页
        path: 'home',
        component: () => import(/* webpackChunkName: "Home" */ '@/views/Home'),
        meta: {
          scrollT: 0 // 保存首页离开时滚动条，滚动条离开的位置
        }
      },
      { // 我的
        path: 'user',
        component: () => import(/* webpackChunkName: "User" */ '@/views/User')
      }
    ]
  },
  { // 搜索组件路由
    path: '/search',
    component: () => import(/* webpackChunkName: "Search" */ '@/views/Search')
  },
  { // 搜索结果路由
    path: '/search_result/:kw',
    component: () => import(/* webpackChunkName: "SearchResult" */ '@/views/Search/SearchResult')
  },
  { // 文章详情路由
    path: '/detail',
    component: () => import(/* webpackChunkName: "ArticleDetail" */ '@/views/ArticleDetail')
  },
  { // 用户编辑个人资料路由
    path: '/user_edit',
    component: () => import(/* webpackChunkName: "UserEdit" */ '@/views/User/UserEdit')
  },
  { // 小思同学聊天界面路由
    path: '/chat',
    component: () => import(/* webpackChunkName: "Chat" */ '@/views/Chat')
  }
]

const router = new VueRouter({
  routes
})

// 全局前置路由守卫————初始化的时候、每次路由切换之前被调用
// router.beforeEach((to, from, next) => {
//   if (getToken()?.length > 0 && to.path === '/login') {
//     next('/layout/home')
//   } else {
//     next()
//   }
// })

// 全局后置路由守卫————初始化的时候被调用、每次路由切换之后被调用
// router.afterEach((to, from) => {
//   // console.log('后置路由守卫',to,from)
//   document.title = to.meta.title || '小新头条'
// })

export default router
