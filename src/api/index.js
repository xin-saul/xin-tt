// 统一封装接口方法
// 每个方法负责请求一个url地址
// 逻辑页面，导入这个接口方法，就能发请求
// 好处：请求url路径，可以在这里统一管理
// Content-Type 是对请求体里数据类型进行声明
// 既引入也同时向外按需导出，所有引入过来的方法
export * from './ArticleDetail' // 文章详情相关
export * from './User' // 用户相关
export * from './Search' // 搜索相关
export * from './Login' // 登录相关
export * from './Home' // 首页(频道)相关, 首页文章列表
