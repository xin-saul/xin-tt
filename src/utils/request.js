// axios封装网络请求
import theAxios from 'axios'
import router from '@/router'
import Notify from '@/ui/Notify'
import { getToken, removeToken, setToken } from './token'
import { getNewTokenAPI } from '@/api'
import { removeStorage } from './storage'
// 新建一个新的axios实例
const axios = theAxios.create({
  baseURL: 'http://toutiao.itheima.net',
  timeout: 20000 // 20秒超时时间（请求20秒无响应直接判定超时）
})

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // http响应状态码为2xx，3xx就进入这里
  // ? 可选链操作符 如果前面的对象没有length 整个表达式原地返回undefined
  // 如果getToken()在原地有token字符串，才能调用length获取长度
  if (getToken()?.length > 0 && config.headers.Authorization === undefined) {
    config.headers.Authorization = `Bearer ${getToken()}`
  }
  // 在发送请求之前做些什么
  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // http响应状态码为4xx，5xx就进入这里
  // 对响应数据做点什么
  return response
}, async function (error) {
  // 对响应错误做点什么
  console.dir(error)
  if (error.response.status === 401) {
    // 不能使用this.$router(因为this不是vue组件对象无法调用$router)
    // 解决：this.$router为了能拿到router路由对象，所以直接引入@/router下的router对象
    // Notify({ type: 'warning', message: '身份过期' })
    removeToken() // 清除token，才能让路由守卫判断失败，放行去登陆页

    // 方式1：强制跳转到登陆，用户有感知
    // router.replace('/login')

    // 方式2：使用refresh_token换回新的token再继续使用，用户无感知
    const res = await getNewTokenAPI()
    // 1.更新本地token
    setToken(res.data.data.token)
    // 2.更新新的token在请求头里
    error.config.headers.Authorization = `Bearer ${res.data.data.token}`
    // 3.未完成这次请求，再一次发起
    // error.config就是上一次请求的配置对象
    // 结果要return回原本逻辑页面调用地方-还是return回去一个Promise对象
    return axios(error.config)
  } else if (error.response.status === 500 && error.config.url === '/v1_0/authorizations' && error.config.method === 'put') {
    removeToken()
    removeStorage('refresh_token')
    Notify({ type: 'warning', message: '身份过期' })
    // router.currentRoute 相当于在vue文件内this.$route -> 当前路由对象信息
    console.log(router.currentRoute)
    // fullPath 路由对象里的完整路由路径
    router.replace(`/login?path=${router.currentRoute.fullPath}`)
  }
  return Promise.reject(error)
})

// 导出自定义函数, 参数对象解构赋值
export default ({ url, method = 'GET', params = {}, data = {}, headers = {} }) => {
  return axios({
    url,
    method,
    params,
    data,
    headers
  })
}
