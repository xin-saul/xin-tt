import request from '@/utils/request'

// 频道 - 获取用户频道（用户没有登陆，无token状态下返回默认频道列表）
export const getUserChannelsListAPI = () => request({
  url: '/v1_0/user/channels',
  method: 'GET'
})

// 频道 - 获取所有频道列表
export const getAllChannelsListAPI = () => request({
  url: '/v1_0/channels',
  method: 'GET'
})

// 频道 - 设置用户频道（重置式）
export const updateChannelsAPI = ({ channels }) => request({
  url: '/v1_0/user/channels',
  method: 'PUT',
  data: {
    channels
  }
})

// 频道 - 删除用户指定的频道
export const removeChannelAPI = ({ target }) => request({
  url: `/v1_0/user/channels/${target}`,
  method: 'DELETE'
})

// 新闻 - 获取文章新闻推荐
// eslint-disable-next-line
export const getArticleAPI = ({ channel_id, timestamp }) => request({
  url: '/v1_0/articles',
  method: 'GET',
  params: {
    channel_id,
    timestamp
  }
})

// 新闻 - 对文章不感兴趣
export const dislikeArticleAPI = ({ artId }) => request({
  url: '/v1_0/article/dislikes',
  method: 'POST',
  data: {
    target: artId
  }
})

// 新闻 - 举报文章
export const reporArticleAPI = ({ target, type }) => request({
  url: '/v1_0/article/reports',
  method: 'POST',
  data: {
    target,
    type,
    remark: '举报一手'
  }
})
