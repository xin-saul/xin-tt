import request from '@/utils/request'
import { getStorage } from '@/utils/storage'

// 登陆 - 登陆接口
export const loginAPI = ({ mobile, code }) => request({
  url: '/v1_0/authorizations',
  method: 'POST',
  data: {
    mobile,
    code
  }
})

// 用户 - 刷新token
export const getNewTokenAPI = () => request({
  url: '/v1_0/authorizations',
  method: 'PUT',
  headers: {
    // 请求拦截器统一携带的是token，而这次请求需要带的是refresh_token
    // axios请求拦截器中的判断，就是为了这种情况准备的
    Authorization: `Bearer ${getStorage('refresh_token')}`
  }
})
